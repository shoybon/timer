export const createStore = (reducer, initialState) => {


  let currentState = initialState;
  let listeners = []; //вместо const let, вроде должно быть изменяемым?

  const getState = () => currentState;

  const dispatch = action => {
    currentState = reducer(currentState, action);
    listeners.forEach(listener => listener());
    // console.log(action);
    // console.log(listeners);
    // console.log(getState());

    return action;
  }

  const subscribe = listener => listeners.push(listener);


  return { getState, dispatch, subscribe };
  
}
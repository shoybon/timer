import { connect } from './app/Connect';
import TimerComponent from './app/components/TimerComponent';

export const Timer = connect(state => ({
        currentInterval: state.interval,
    }), 
    () => { }
)(TimerComponent);